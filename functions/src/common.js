const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');


const OTHER_CONFIGS = require("../res/TournamentConfigs.json")


const DELTADNA_ENVIRONMENT_KEY = OTHER_CONFIGS.DELTADNA_ENVIRONMENT_KEY
const DELTADNA_COLLECT_URL = OTHER_CONFIGS.DELTADNA_COLLECT_URL



firebaseAdmin.initializeApp();  



module.exports = {
  firebaseAdmin,
  DELTADNA_COLLECT_URL,
  DELTADNA_ENVIRONMENT_KEY,
}



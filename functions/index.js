const functions = require('firebase-functions');
const testFunctions = require('./src/testFunctions');




const runtimeOptsMedium = {
  timeoutSeconds: 360,
  memory: '512MB'
}




//Test Functions
exports.postDDNAEvent = functions.runWith(runtimeOptsMedium).https.onRequest(testFunctions.postDDNAEvent);
